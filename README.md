Movieclub
=========

The easy way to setup you server for a distributed watch party.

Thanks to Dražen Lučanin (@metakermit) for providing initial research.

Getting started
---------------

1. Get a virtual server with linux (eg. from Digital Ocean, Scaleway)
2. Clone this repository and cd into it
3. Install the depndencies:

```bash
./bin/install.sh
```

4. `cp .env.example .env` and adjust your variables
5. In the repository directory run `docker-compose up -d`
6. Copy the file to stream to the server
7. Generate a password and links:

```bash
source ./bin/generate-links.sh
```

8. Preencode the file for streaming:

```bash
ffmpeg -i /path/to/file.mp4 -c:v libx264 -preset medium -maxrate 3000k -bufsize 6000k  -g 50 -c:a aac -b:a 128k -ac 2 -ar 44100 -strict -2 /path/to/file.flv
```
Depending on your server this will take approximately 20 - 30% of the movie's duration.

9. Distribute the links to your friends over [Signal](https://signal.org/)
10. Start playing the movie:

```bash
./bin/play-movie.sh /path/to/file.flv
```

11. Chat on Signal about the movie
12. Only stream movies that are in the public domain and don't say I didn't warn you!
13. You could start with Sergei Eisensteins classic **[Battleship Potemkin](https://archive.org/details/BattleshipPotemkin)**
 
More Infos
----------

- [FFmpeg - Encoding for Streaming Sites](https://trac.ffmpeg.org/wiki/EncodingForStreamingSites)
- [FFmpeg - How to Burn Subtitles into Video](https://trac.ffmpeg.org/wiki/HowToBurnSubtitlesIntoVideo)

Todos
-----

### Short Term

- [ ] Add per stream chat
- [x] Document pretranscoding settings
- [ ] Allow setting stylesheet for watch page
- [ ] Allow setting title for watch page
- [ ] Create stream script that returns the watch URL
- [ ] Update traefik config to 2.0


### Long Term

- [ ] Switch to bittorrent like streaming protocol (PeerTube or similar)
