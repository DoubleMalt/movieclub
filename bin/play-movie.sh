#!/bin/bash

source .env

STREAM_ID=$(pwgen -B 6 1)

echo "now streaming file ${1} on:"
echo
echo "Web link: https://${MOVIECLUB_DOMAIN}/#${STREAM_ID}"
echo "Mobile link: https://${MOVIECLUB_DOMAIN}/live/${STREAM_ID}.m3u8"
echo

ffmpeg -re -i $1 -c copy -f flv rtmp://localhost/stream/${STREAM_ID}
