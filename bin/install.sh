#!/bin/bash

# install docker as per https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script
sudo apt remove docker docker-engine docker.io containerd runc
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
# If you would like to use Docker as a non-root user, you should now consider adding your user to the “docker” group with something like:
# sudo usermod -aG docker your-user

# install docker-compose as per https://docs.docker.com/compose/install/
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

sudo apt install pwgen
sudo apt install ffmpeg
