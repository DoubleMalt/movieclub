#!/bin/bash

export STREAM_ID=$(pwgen 6 1)
source .env
echo "Web link: https://${MOVIECLUB_DOMAIN}/#${STREAM_ID}"
echo "Mobile link: https://${MOVIECLUB_DOMAIN}/live/${STREAM_ID}"
